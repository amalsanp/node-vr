const helper = require("../helper");
const acl = require("./acl");

module.exports.permit = function () {
  return (req, res, next) => {
    let url =
      req._parsedUrl.pathname
        .replace(/\/[0-9a-f]{24}/g, "/id")
        .split("/")
        .slice(1)
        .join(".") + `.${req.method}`;
    let allowed = helper.manageObj(acl, url);
    console.log(allowed);
    /**
     * function Check if user has permission
     * @param {*} role
     * @return {*}
     */
    const isAllowed = (role) => allowed.indexOf(role) > -1;
    /**
     * Check if user exist and has permission
     */

    if (req.user && allowed && isAllowed(req.user.role)) {
      console.log(req.user.role);
      next();
    }
    // role is allowed, so continue on the next middleware
    else {
      console.log(url);
      console.log(helper.manageObj(acl, url));
      console.log(req.user.role);
      res.status(403).json({ _code: 403, _message: "Access Denied" }); // user is forbidden
    }
  };
};
