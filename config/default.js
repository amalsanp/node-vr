const PORT = process.env.PORT || 3000;
const local = "http://localhost";
const HOST = process.env.HOST || local;
module.exports = {
  app: {
    name: "Server V1",
    port: PORT,
    baseUrl: `${HOST}:${PORT}`,
  },
  api: {
    prefix: "/api/",
    versions: ["v1.0"],
  },
  lang: "en",
  authToken: {
    superSecret: "ipa-odot",
    secret: "ed1371ad587eb338b876159434a68f34",
    // expiresIn: 86400,
    expiresIn: 15778800000, //6 month
  },
  db: {
    url:
      process.env.MONGO_URL ||
      "mongodb+srv://amalsan:Amalpass@cluster0.e8mbajn.mongodb.net/vr?retryWrites=true&w=majority",
  },
  //   smtp: {
  //     host: "mail.supremecluster.com",
  //     port: 465,
  //     secure: true,
  //     auth: {
  //       user: "webadmin@widerangegroup.me",
  //       pass: "kKnAYy610$",
  //     },
  //   },
  //   sendGrid: {
  //     key: "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",
  //   },
  orderMail: "amal@xltech.in",
  cacheTime: 5,
  taxPercentage: 5,
  itemsPerPage: 8,
  passwordRegex: "/^([A-Za-zd!@#$&*~]).{7,20}$/", //minimum 8 character letters or digits.
  phoneNumberRegex: "^[-s./0-9]{6,12}$",
  phoneCodeRegex: "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}$",
  host: HOST,
};
