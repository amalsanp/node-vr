const appService = require("./app");
const jwt = require("jsonwebtoken");
const config = require("config");

module.exports.login = async (Model, obj) => {
  try {
    const { email, password } = obj;
    let user = await appService.findOne(Model, { email: email });
    if (!user) {
      throw new Error("Incorrect email");
    }
    const data = await user.comparePassword(password);
    if (data) {
      let payload = {
        id: user._id,
        role: user.role,
        email: user.email,
        name: user.firstName,
      };
      const signedToken = await jwt.sign(
        payload,
        config.get("authToken.secret"),
        { expiresIn: 36000 }
      );
      if (!signedToken) {
        throw new Error("No token");
      }
      return { user, token: `Bearer ${signedToken}` };
    }
    throw new Error("Incorrect password");
  } catch (e) {
    throw e;
  }
};

module.exports.register = async (Model, obj) => {
  try {
    let user = await appService.createOne(Model, obj);
    if (!user) {
      throw new Error("User registration fail");
    }
    return user;
  } catch (e) {
    throw e;
  }
};
