const { isValidDoc } = require("../helper");

module.exports.findOne = async (Model, params = {}) => {
  return await Model.findOne(params);
};

module.exports.createOne = async (Model, body) => {
  const obj = new Model(body);
  const valid = isValidDoc(obj);
  if (valid["ok"]) {
    return await obj.save();
  }
  throw new Error(JSON.stringify(valid));
};
