const { MESSAGES } = require("../helper/constants");
const User = require("../models/User");
const userService = require("../services/user");

module.exports.login = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    const user = await userService.login(User, { email, password });
    return res.jsonSuccess(MESSAGES("LOGIN", true), 200, user);
  } catch (e) {
    return res.jsonError(MESSAGES("LOGIN", false), 400, {
      message: e,
    });
  }
};

module.exports.register = async (req, res, next) => {
  try {
    const body = req.body;
    const user = await userService.register(User, body);
    return res.jsonSuccess(MESSAGES("REGISTER", true), 200, user);
  } catch (e) {
    return res.jsonError(MESSAGES("REGISTER", false), 400, {
      message: e,
    });
  }
};
