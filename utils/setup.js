const mongoose = require("mongoose");
const config = require("config");
// const q = require("q");

module.exports.initialize = () => {
  // mongoose.Promise = q.Promise;
  mongoose.connection.on("open", () => {
    // '\x1b[32m%s\x1b[0m' for coloring console
    console.log("\x1b[32m%s\x1b[0m", "Connected to mongo shell.");
    console.log("\x1b[32m%s\x1b[0m", `mongodb url ${config.get("db.url")}`);
  });
  mongoose.connection.on("error", (err) => {
    // '\x1b[32m%s\x1b[0m' for coloring console
    console.log("\x1b[31m%s\x1b[0m", "Could not connect to mongo server!");
    console.log(err);
  });
  mongoose.connect(config.get("db.url"), {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
};
