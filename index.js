const express = require("express");
const http = require("http");
const config = require("config");
const passport = require("passport");
const bodyParser = require("body-parser");
const routers = require("./routes");
const path = require("path");
const app = express();
const ResponseController = require("./controllers/responseController");
const Setup = require("./utils/setup");
const cors = require("cors");
// const qt = require("quickthumb");

Setup.initialize(config);
app.disable("x-powered-by");
app.set("port", config.get("app.port"));
app.use(cors({ origin: "http://localhost:3001", credentials: true }));

app.use(passport.initialize());
require("./passport")(passport);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// app.use("/uploads", express.static(path.join(__dirname, "uploads")));

// app.use("/uploads", qt.static(path.join(__dirname, "uploads")));

app.use(config.get("api.prefix") + config.get("api.versions")[0], routers);

// app.use(express.static(path.join(__dirname, "AdminApp/build")));

// root route
// app.get("*", function (req, res) {
//   res.sendFile(path.join(__dirname, "AdminApp/build", "index.html"));
// });

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  console.log(
    "------------------error---------------------\n",
    err,
    "\n---------------------------------------"
  );
  return res
    .status(400)
    .json(
      ResponseController.jsonError("Server Error ", err.status || 500, err)
    );
});

const server = http.createServer(app);
const port = app.get("port");
server.listen(port, () => {
  // '\x1b[32m%s\x1b[0m' for coloring console
  console.log(
    "\x1b[32m%s\x1b[0m",
    `Application listening on ${config.get("app.baseUrl")}`
  );
  console.log(
    "\x1b[32m%s\x1b[0m",
    `Environment => ${config.util.getEnv("NODE_ENV")}`
  );
});

module.exports = app;
