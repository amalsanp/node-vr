const express = require("express");
const { isValidDoc } = require("../helper");
const User = require("../models/User");
const router = express.Router();
const jwt = require("jsonwebtoken");
const config = require("config");
const userController = require("../controllers/user");

router.route("/login").post(userController.login);
router.route("/register").post(userController.register);

module.exports = router;
