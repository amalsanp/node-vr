const mongoose = require("mongoose");
const { Schema } = mongoose;
const bcrypt = require("bcryptjs");
const { toJSON } = require("./plugins");

const UserSchema = new Schema(
  {
    firstName: {
      type: String,
      required: true,
      validate: {
        validator: function (v) {
          return v.length > 2;
        },
        message: (props) => `${props.value} is not a valid firstname`,
      },
    },
    lastName: {
      type: String,
      validate: {
        validator: function (v) {
          return v.length > 2;
        },
        message: (props) => `${props.value} is not a valid lastName`,
      },
    },
    email: {
      type: String,
      unique: true,
      required: true,
      validate: {
        validator: function (v) {
          return /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(v);
        },
      },
    },
    password: {
      type: String,
      validate: {
        validator: function (v) {
          return v.length > 3;
        },
      },
      message: "Password should have minimum 8 length",
    },
    role: {
      type: String,
      default: "USER",
      enum: {
        values: ["ADMIN", "USER"],
        message: "{VALUE} is not supported",
      },
    },
  },
  {
    timestamps: true,
  }
);

UserSchema.plugin(toJSON);

UserSchema.pre("save", function (next) {
  var user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified("password")) return next();

  // generate a salt
  const saltNum = parseInt(process.env.SALT_WORK_FACTOR);
  bcrypt.genSalt(saltNum, function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);
      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.comparePassword = async function (candidatePassword, cb) {
  return await bcrypt.compare(candidatePassword, this.password);
  // bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
  //   if (err) return cb(err);
  //   cb(null, isMatch);
  // });
};

UserSchema.methods.isAdmin = function (email, cb) {
  return this.role == "ADMIN";
};

const User = mongoose.model("User", UserSchema);

module.exports = User;
