let JWTStrategy = require("passport-jwt").Strategy;
let ExtractJwt = require("passport-jwt").ExtractJwt;
const jwt = require("jsonwebtoken");
const config = require("config");

let opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.get("authToken.secret");

module.exports = (passport) => {
  passport.use(
    new JWTStrategy(opts, async (jwtPayload, done) => {
      console.log(jwtPayload);
      if (jwtPayload?.id) {
        return done(null, jwtPayload);
      }
      console.log("bad");
      return done(null, jwtPayload);
    })
  );
};
