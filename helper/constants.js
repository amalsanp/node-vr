const MESSAGES = (path, status) => {
  return status ? `${path} SUCCESS` : `${path} FAIL`;
};

module.exports.MESSAGES = MESSAGES;
